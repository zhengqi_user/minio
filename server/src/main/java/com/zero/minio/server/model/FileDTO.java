package com.zero.minio.server.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.PrimitiveIterator;

/**
 * @author zero
 */
@Data
@Builder
public class FileDTO implements Serializable {

    private String uuid ;
    private String  md5;
    private Boolean  uploaded ;
    private String uploadID  ;
    private String chunks;
    private Integer totalChunkCounts  ;
    private Integer chunkNumber;
    private Long size;
    private String  fileName;
    private String  completedParts	;
    private String url;
    private String etag;
}
