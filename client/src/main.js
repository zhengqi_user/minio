import { createApp } from 'vue'
import ADV from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import App from './App.vue'
import router from './router'
import store from './store'
createApp(App).use(store).use(router).use(ADV).mount('#app')
